package main

type ContactMessageStructure struct {
	Email     string `json:"email,omitempty"`
	Firstname string `json:"firstname,omitempty"`
	Lastname  string `json:"lastname,omitempty"`
	Message   string `json:"message,omitempty"`
}
