package main

import (
	"bitbucket.org/zuful/tripozon-protobuf/aesir"
	"bitbucket.org/zuful/tripozon-protobuf/fortinbras"
	"cloud.google.com/go/firestore"
	"context"
	"encoding/json"
	"firebase.google.com/go"
	"firebase.google.com/go/messaging"
	uuid "github.com/satori/go.uuid"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
	"log"
)

var establishmentsCollectionName string = "establishments"
var employeesCollectionName string = "employees"
var conversationsCollectionName string = "conversations"
var messagesCollectionName string = "messages"
var contactMessagesCollectionName string = "contact"

var app *firebase.App
var firebaseErr error
var firestoreClient *firestore.Client
var ctx context.Context

func initAesir() {

	//Connection to Firebase
	ctx = context.Background()
	opt := option.WithCredentialsFile("messenger-1670c.json")
	app, firebaseErr = firebase.NewApp(context.Background(), nil, opt)
	if firebaseErr != nil {
		log.Println(firebaseErr)
	}

	//Firestore
	firestoreClient, firebaseErr = app.Firestore(ctx)
	if firebaseErr != nil {
		log.Println(firebaseErr)
	}
}

func getAllEmployees(establishmentId string) []*fortinbras.EmployeeStructure {

	var allEmployees []*fortinbras.EmployeeStructure = make([]*fortinbras.EmployeeStructure, 0)

	res := firestoreClient.Collection(establishmentsCollectionName).Doc(establishmentId).
		Collection(employeesCollectionName).
		Where("CanChat", "==", true).
		Documents(ctx)

	defer res.Stop()
	for {
		var employee *fortinbras.EmployeeStructure
		employeeSnap, err := res.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Println(err)
		}

		employeeMap := employeeSnap.Data()
		employeeJson, err := json.Marshal(employeeMap)
		if err != nil {
			log.Println(err)
		}

		err = json.Unmarshal([]byte(employeeJson), &employee)
		if err != nil {
			log.Println(err)
		}

		allEmployees = append(allEmployees, employee)
	}

	return allEmployees
}

func getEmployeeIdToAssignConversation(establishmentId string, allEmployees []*fortinbras.EmployeeStructure) string {

	var employeeIdToAssignConversation string
	var allEmployeesConversationsCount []*aesir.EmployeeConversationsCountStructure = getAllEmployeesConversationsCount(establishmentId, allEmployees)

	if len(allEmployees) == 0 { //if there are no employees that can chat (or no employees at all)
		employeeIdToAssignConversation = ""
	} else if len(allEmployeesConversationsCount) == 1 { //if only one employee that has conversations we directly assign to him
		employeeIdToAssignConversation = allEmployeesConversationsCount[0].GetEmployeeId()
	} else {

		var previousCount int32 = allEmployeesConversationsCount[0].GetConversationsCount() //we assign the previous count with the first value of the slice

		for _, oneEmployeeConversationsCount := range allEmployeesConversationsCount {

			if oneEmployeeConversationsCount.GetConversationsCount() < previousCount { //
				employeeIdToAssignConversation = oneEmployeeConversationsCount.GetEmployeeId()
			}

			previousCount = oneEmployeeConversationsCount.GetConversationsCount()
		}

		if previousCount == 0 { //If none of the employees has a conversation assigned
			employeeIdToAssignConversation = allEmployeesConversationsCount[0].GetEmployeeId()
		}
	}

	return employeeIdToAssignConversation
}

func getAllEmployeesConversationsCount(establishmentId string, allEmployees []*fortinbras.EmployeeStructure) []*aesir.EmployeeConversationsCountStructure {

	var allEmployeesConversationsCount []*aesir.EmployeeConversationsCountStructure = make([]*aesir.EmployeeConversationsCountStructure, 0)

	for _, oneEmployee := range allEmployees {

		var oneEmployeeConversationsCount aesir.EmployeeConversationsCountStructure = aesir.EmployeeConversationsCountStructure{
			EmployeeId:         oneEmployee.GetId(),
			ConversationsCount: getOneEmployeeConversationsCount(establishmentId, oneEmployee.Id),
		}

		allEmployeesConversationsCount = append(allEmployeesConversationsCount, &oneEmployeeConversationsCount)
	}

	return allEmployeesConversationsCount
}

func getOneEmployeeConversationsCount(establishmentId string, employeeId string) int32 {

	var conversationsCount int32

	res := firestoreClient.Collection(conversationsCollectionName).
		Where("EstablishmentId", "==", establishmentId).
		Where("EstablishmentEmployeeId", "==", employeeId).
		Documents(ctx)

	defer res.Stop()

	oneEmployeeAllConversationsSnap, err := res.Next()
	if err != nil {
		log.Println(err)
	}

	if oneEmployeeAllConversationsSnap != nil { //avoid nil variable error when callint Data()
		conversationsCount = int32(len(oneEmployeeAllConversationsSnap.Data()))
	}

	return conversationsCount
}

func getOneUserAllConversations(establishmentId string, employeeId string) []*aesir.ConversationStructure {

	var allConversations []*aesir.ConversationStructure = make([]*aesir.ConversationStructure, 0)

	res := firestoreClient.Collection(conversationsCollectionName).
		Where("EstablishmentId", "==", establishmentId).
		Where("EstablishmentEmployeeId", "==", employeeId).
		Documents(ctx)

	defer res.Stop()

	for {
		var conversation *aesir.ConversationStructure
		conversationSnap, err := res.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Println(err)
		}

		conversationMap := conversationSnap.Data()
		conversationJson, err := json.Marshal(conversationMap)
		if err != nil {
			log.Println(err)
		}

		err = json.Unmarshal([]byte(conversationJson), &conversation)
		if err != nil {
			log.Println(err)
		}

		allConversations = append(allConversations, conversation)
	}

	return allConversations
}

func getOneConversationUnseenMessageCount(conversationId string) int {

	var oneConversationUnseenMessageCount int

	res := firestoreClient.Collection(conversationsCollectionName).
		Doc(conversationId).
		Collection(messagesCollectionName).
		Where("Seen", "==", false).
		Documents(ctx)
	defer res.Stop()

	oneConversationCountUnseenMessagesSnap, err := res.Next()
	if err != nil {
		log.Println(err)
	}

	if oneConversationCountUnseenMessagesSnap != nil { //avoid nil variable error when callint Data()
		oneConversationUnseenMessageCount = len(oneConversationCountUnseenMessagesSnap.Data())
	}

	return oneConversationUnseenMessageCount
}

func getLastMessage(conversationId string) aesir.MessageStructure {

	dir := firestore.Direction(2) //DESC
	lastSentMessage := firestoreClient.Collection(conversationsCollectionName).
		Doc(conversationId).
		Collection(messagesCollectionName).
		Limit(1).
		OrderBy("creationDate", dir).
		Documents(ctx)
	defer lastSentMessage.Stop()

	var message aesir.MessageStructure

	for {
		messageSnap, err := lastSentMessage.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Println(err)
		}

		messageMap := messageSnap.Data()
		messageJson, err := json.Marshal(messageMap)
		if err != nil {
			log.Println(err)
		}

		err = json.Unmarshal([]byte(messageJson), &message)
		if err != nil {
			log.Println(err)
		}
	}

	return message
}

func cleanUnseenMessages(conversationId string) []*firestore.WriteResult {

	var update []firestore.Update = []firestore.Update{
		{Path: "seen", Value: true},
	}

	unseenMessages := firestoreClient.Collection(conversationsCollectionName).
		Doc(conversationId).
		Collection(messagesCollectionName).
		Where("seen", "==", false).Documents(ctx)
	defer unseenMessages.Stop()
	batch := firestoreClient.Batch()

	for {

		var message aesir.MessageStructure

		messageSnap, err := unseenMessages.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Println(err)
		}

		messageMap := messageSnap.Data()
		messageJson, err := json.Marshal(messageMap)
		if err != nil {
			log.Println(err)
		}

		err = json.Unmarshal([]byte(messageJson), &message)
		if err != nil {
			log.Println(err)
		}

		message.Seen = true

		batch.Update(messageSnap.Ref, update)
	}

	res, err := batch.Commit(ctx)
	if err != nil {
		log.Println(err)
	}

	return res
}

func getOneEmployeeFromSlice(employeeId string, allEmployees []*fortinbras.EmployeeStructure) *fortinbras.EmployeeStructure {

	var employee *fortinbras.EmployeeStructure

	for _, oneEmployee := range allEmployees {

		if oneEmployee.GetId() == employeeId {
			employee = oneEmployee
		}

	}

	return employee
}

func sendFcmNotification(notification NotificationStructure) string {

	var message messaging.Message = messaging.Message{
		Token: notification.FcmToken,
		Notification: &messaging.Notification{
			Title: notification.Title,
			Body:  notification.Body,
		},
		Webpush: &messaging.WebpushConfig{
			Notification: &messaging.WebpushNotification{
				Title: notification.Title,
				Icon:  notification.Icon,
			},
		},
	}

	fcmMessaging, err := app.Messaging(ctx)
	if err != nil {
		log.Println(err)
	}

	send, err := fcmMessaging.Send(ctx, &message)
	if err != nil {
		log.Println(err)
	}

	return send
}

func saveContactMessage(contactMessageStructure ContactMessageStructure) *firestore.WriteResult {

	var contactMessageId string = uuid.NewV4().String()
	res, err := firestoreClient.Collection(contactMessagesCollectionName).Doc(contactMessageId).Set(ctx, contactMessageStructure)
	if err != nil {
		log.Println(err)
	}

	return res
}
