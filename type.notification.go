package main

type NotificationStructure struct {
	FcmToken string `json:"fcmToken,omitempty"`
	Title    string `json:"title,omitempty"`
	Body     string `json:"body,omitempty"`
	Icon     string `json:"icon,omitempty"`
}
