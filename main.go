package main

import (
	"bitbucket.org/zuful/tripozon-protobuf/fortinbras"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"log"
	"time"
)

func main() {

	r := gin.Default()
	// CORS for https://foo.com and https://github.com origins, allowing:
	// - PUT and PATCH methods
	// - Origin header
	// - Credentials share
	// - Preflight requests cached for 12 hours

	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{
			"http://localhost:8100",
			"http://localhost:4200",
			"https://messenger-1670c.firebaseapp.com",
			"https://tripozon-web.com",
			"https://tripozon-pro.firebaseapp.com"},
		AllowMethods:     []string{"PUT", "PATCH"},
		AllowHeaders:     []string{"Origin", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length", "Content-Type"},
		AllowCredentials: true,
		/*AllowOriginFunc: func(origin string) bool {
			return origin == "https://github.com"
		},*/
		MaxAge: 12 * time.Hour,
	}))

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	r.GET("/employee-to-assign-conversation/:establishmentId", func(c *gin.Context) {
		var establishmentId string = c.Param("establishmentId")
		var allEmployees []*fortinbras.EmployeeStructure = getAllEmployees(establishmentId)
		var employeeIdToAssignConversation string = getEmployeeIdToAssignConversation(establishmentId, allEmployees)
		var employee *fortinbras.EmployeeStructure = getOneEmployeeFromSlice(employeeIdToAssignConversation, allEmployees)

		c.JSON(200, employee)
	})

	r.GET("/last-sent-message/:conversationId", func(c *gin.Context) {
		var conversationId string = c.Param("conversationId")
		lastSentMessage := getLastMessage(conversationId)

		c.JSON(200, lastSentMessage)
	})

	r.GET("/clean-unseen-messages/:conversationId", func(c *gin.Context) {
		var conversationId string = c.Param("conversationId")
		res := cleanUnseenMessages(conversationId)

		c.JSON(200, res)
	})

	r.POST("/send-notification", func(c *gin.Context) {
		var notification NotificationStructure

		//prevents from unmarshalling json and binds the route param directly to the type
		err := c.Bind(&notification)
		if err != nil {
			log.Println(err)
		}

		res := sendFcmNotification(notification)
		c.JSON(200, res)
	})

	r.POST("/send-contact-message", func(c *gin.Context) {
		var contactMessage ContactMessageStructure

		//prevents from unmarshalling json and binds the route param directly to the type
		err := c.Bind(&contactMessage)
		if err != nil {
			log.Println(err)
		}

		res := saveContactMessage(contactMessage)
		c.JSON(200, res)
	})

	//r.Use(cors.Default())//allows all origins (must but changed when put in production)
	r.Run()
}

func init() {
	initAesir()
}
